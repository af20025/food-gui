import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {
    int a=0;
    void order(String food){
        textPane2.setText("Total  " +a+ " yen");
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " +food+ "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation == 0){
            String currentText = textPane1.getText();
            textPane1.setText(currentText + food + "\n");
            textPane2.setText("Total  " +a+ " yen");
            JOptionPane.showMessageDialog(null, "Thank you for ordering" +food+ " ! It will be served as soon as possible.");
        }
    }
    private JPanel root;
    private JButton tempuraButton;
    private JButton ramenButton1;
    private JButton udonButton;
    private JTextPane textPane1;
    private JButton yakisobaButton;
    private JButton karaageButton;
    private JButton gyozabutton;
    private JButton checkoutButton;
    private JLabel textPane2;
    private JLabel label1;

    public FoodGUI() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                a=a+10;
                order("Tempura");
            }
        });
        tempuraButton.setIcon(new ImageIcon(
                this.getClass().getResource("tempura.jpg")
        ));
        ramenButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                a=a+20;
                    order("Ramen");
            }
        });
        ramenButton1.setIcon(new ImageIcon(
                this.getClass().getResource("ramen.jpg")
        ));
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                a=a+30;
                    order("Udon");
            }
        });
        udonButton.setIcon(new ImageIcon(
                this.getClass().getResource("udon.jpg")
        ));
        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                a=a+40;
                order("Yakisoba");
            }
        });
        yakisobaButton.setIcon(new ImageIcon(
                this.getClass().getResource("yakisoba.jpg")
        ));
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                a=a+50;
                order("Karaage");
            }
        });
        karaageButton.setIcon(new ImageIcon(
                this.getClass().getResource("karaage.jpg")
        ));
        checkoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if(confirmation == 0){
                    textPane1.setText(" ");
                    JOptionPane.showMessageDialog(null, "Thank you. The total price is " +a+ " yen");
                    a=0;
                    textPane2.setText("Total  " +a+ " yen");
                }
            }
        });
        gyozabutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                a=a+60;
                order("Gyoza");
            }
        });
        gyozabutton.setIcon(new ImageIcon(
                this.getClass().getResource("gyoza.jpg")
        ));
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
